# Intro

In order to make sharing my PowerShell profile across machines I wanted to put
it in a Git repo. I decided to move it out of `~\Documents\WindowsPowerShell` to
make things a bit cleaner for myself. You may not agree with this pattern and 
that's fine, this is just how I decided to do it :)

# The ACTUAL Profile

Since my profile is going to live in a git repo, and since I may want slightly
different things based on the machines I'm on (work vs personal) I decided to 
keep the default profile for PowerShell, and just implement it in such a way that
it would load the *actual* profile I wanted on that machine.

Here's my PS Profile, for reference in case you decide to clone this repository:

```
$userPath = $env:userprofile
$libHome = $userPath + "\PS"

##-------------------------------------------
## Load Script Libraries
##-------------------------------------------
Get-ChildItem $libHome -Filter *.ps1 |
  Where-Object { $_.Extension -eq '.ps1' } | 
  ForEach-Object {
    if(-Not ((Get-Item $_.FullName) -is [System.IO.DirectoryInfo])) {
        . $_.FullName
    }
}
```

This will simply load any scripts that I have in `~\PS`. This is handy because it
makes it easy to simply drop scripts in that directory, reload my profile and go.
However, you do have to be careful with load order. This works on Alphabetical 
order. So, for instance, `automator.ps1` would load before `profile.ps1`, which 
is why I've broken out some things into directories.