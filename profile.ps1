Write-Output "Loading Profile..."

$userPath = $env:userprofile
$libHome = $userPath + "\PS"
$functions = $libHome + "\functions"

# PATH
$env:Path += ";C:\tools"
$env:Path += ";C:\Program Files\Git\cmd"
$env:Path += ";C:\Program Files\Git\usr\bin"
$env:Path += ";C:\ProgramData\chocolatey\lib\python3\tools\Scripts"
$env:Path += ";$userPath\AppData\Roaming\Keybase"

# Import oh-my-posh and dependecies
Import-Module posh-git
Import-Module oh-my-posh
Set-Theme pure

# Load functions
. "$functions\git.ps1"
. "$functions\tools.ps1"

# Some Aliases
New-Alias python3 python

# SSH Stuff
Set-Alias ssh-agent "$env:ProgramFiles\git\usr\bin\ssh-agent.exe"
Set-Alias ssh-add "$env:ProgramFiles\git\usr\bin\ssh-add.exe"

# Go Stuff
$goPath = "C:\Users\aaron.crowder\Projects\go"
$env:GOPATH = "$goPath"
$env:GO111MODULE = "on"
$env:Path += ";$goPath\bin"