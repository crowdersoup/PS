function Edit-File ($file_path) {
    code $file_path
}

function Edit-Profile () {
    Edit-File($PROFILE)
}

function Create-Cert ($certHost)
{
    New-SelfSignedCertificate -certstorelocation cert:\localmachine\my -dnsname $certHost
}

function Explore() 
{
    $path = (Resolve-Path .\).Path
    Write-Output "Opening $path ..."
    explorer.exe "$path"
}