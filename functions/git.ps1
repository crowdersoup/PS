# Functions to do git things

Function gup {
    git fetch; git merge --ff-only
}

Function gac {
    git add . ; git commit $args[0 .. ($args.count-1)]
}

Function gck {
    git checkout $args[0 .. ($args.count-1)]
}

Function grb {
    git fetch; git rebase origin/dev
}

Function gip {
    git push $args[0 .. ($args.count-1)]
}

Function gs {
    git status
}